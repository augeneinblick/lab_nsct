
var cluster = require('cluster');

if (cluster.isMaster) {
    var numCPUs = require('os').cpus().length;
    console.log('numCPUs', numCPUs);

    //numCPUs = 1;
    for (var i = 0; i < numCPUs; i++) {
        cluster.fork();
    }
} else {
    var express = require('express');
    var app = express();

    app.get('/', function(req, res) {
        for (var a = 0; a < 999999; a++) {
            // this is pretty wasteful
        }
        res.send('Hello World!');
    });

    var server = app.listen(3000, '192.168.1.54', function() {
        console.log('started on port 3000');
    });
}